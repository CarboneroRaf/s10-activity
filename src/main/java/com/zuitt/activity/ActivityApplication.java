package com.zuitt.activity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class ActivityApplication {

	public static void main(String[] args) {
		SpringApplication.run(ActivityApplication.class, args);
	}

	//Create a user and displays a "new user created" message when a user is created
	@PostMapping("/users")
	public String createUser(){
		return "New user created";
	}

	//Using @GetMapping to retrieve data, when a users are retrieved it will display the "All users retrieved message"
	@GetMapping("/users")
	public String users(){
		return "All users retrieved";
	}

	//Retrieveing a specific user by adding the @PathVariable annotation for the id
	@GetMapping("/users/{id}")
	public String getUser(@PathVariable Long id){
		return "Getting the details of the user with the id " +id;
	}


	//Updates the user by creating another java class
	@PutMapping("/users/{id}")
	@ResponseBody
	public User updateUser(@PathVariable Long id, @RequestBody User name){
		return name;
	}

	//Deletes a user
	//Using @Pathvariable annotation to get the user id in the path and @RequestHeader for the authorization
	//addded a condition that if the header with the "Authorization" key is empty or blank it will display the "Unauthorized access message",
	@DeleteMapping("/users/{id}")
	public String deleteUser(@PathVariable Long id, @RequestHeader(value = "Authorization") String authorization){
		if (authorization == null || authorization.isBlank()){
			return "Unauthorized access";
		}else {
			return "The user " +id+ " has been deleted";
		}
	}

}
