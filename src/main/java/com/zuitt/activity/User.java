package com.zuitt.activity;

public class User {

    //User properties
    private String name;

    //Constructors
    public User(){

    };

    public User(String name){
        this.name = name;
    }

    //Getters
    public String getName(){
        return name;
    }

}
